import { ConfigService } from '@nestjs/config';
import * as fs from 'fs';
import { chunk } from 'lodash';
import { Command, CommandRunner, Option } from 'nest-commander';
import { StorageConfigInput } from '../input/StorageConfig.input';
import { LogService } from '../log.service';
import { BaseCommand } from './base.command';

interface CommandOptions {
  source: StorageConfigInput;
  target: StorageConfigInput;
  output?: string;
}

@Command({
  name: 'differences',
  description: 'Get differences between two storage',
})
export class DifferencesCommand extends BaseCommand implements CommandRunner {
  constructor(
    protected readonly logService: LogService,
    protected readonly configService: ConfigService,
  ) {
    super(logService, configService);
  }

  async execute(passedParam: string[], options: CommandOptions): Promise<void> {
    if (!options.output) {
      this.logService.error('output file required');
    }
    const writeStream = fs.createWriteStream(options.output as string, 'utf-8');

    const { source, target } = this.createSourceAndTargetStorage(
      options.source,
      options.target,
    );

    this.logService.debug('begin processing');
    for await (const files of source.filesIterable()) {
      const processChunks = chunk(files, 100);
      for (let i = 0; i < processChunks.length; i++) {
        const processChunk = processChunks[i];
        await Promise.all(
          processChunk.map(async (file) => {
            try {
              if (options.source.exclude) {
                const shouldPass = options.source.exclude.every(
                  (ex) => file.path.indexOf(ex) === -1,
                );
                if (!shouldPass) {
                  this.logService.debug(`${file.path} excluded`);
                  return;
                }
              }

              this.logService.debug(`checking ${file.path} existence`);
              const hasFile = await target.hasFile(file);
              if (hasFile) {
                this.logService.debug(`file ${file.path} exists`);
                return;
              }
              this.logService.log(`${file.path} not exist`);
              writeStream.write(`${file.path}\n`);
            } catch (error) {
              this.logService.error(`error while uploading ${file.path}`);
              this.logService.error(error, error.stack);
            }
          }),
        );
      }
    }
    writeStream.end();
  }

  @Option({
    flags: '-o, --output [output]',
    description: 'output file',
  })
  getOutputPath(path: string): string {
    return path;
  }
}
