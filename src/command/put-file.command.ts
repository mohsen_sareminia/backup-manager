import { ConfigService } from '@nestjs/config';
import { Command, CommandRunner, Option } from 'nest-commander';
import { StorageConfigInput } from '../input/StorageConfig.input';
import { LogService } from '../log.service';
import { BaseCommand } from './base.command';

interface CommandOptions {
  source: StorageConfigInput;
  target: StorageConfigInput;
  path: string;
}

@Command({ name: 'put-file', description: 'put single file to target' })
export class PutFileCommand extends BaseCommand implements CommandRunner {
  constructor(
    protected readonly logService: LogService,
    protected readonly configService: ConfigService,
  ) {
    super(logService, configService);
  }

  async execute(passedParam: string[], options: CommandOptions): Promise<void> {
    const { source, target } = this.createSourceAndTargetStorage(
      options.source,
      options.target,
    );

    const file = await source.getFile(options.path);
    await target.putFile(file);
  }

  @Option({
    flags: '-p, --path [path]',
    description: 'file path',
  })
  getFilePath(path: string): string {
    return path;
  }
}
