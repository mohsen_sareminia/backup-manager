import { ConfigService } from '@nestjs/config';
import { Command, CommandRunner } from 'nest-commander';
import { StorageFile } from '../entity/StorageFile';
import { Storage } from '../repository/Storage';
import { StorageConfigInput } from '../input/StorageConfig.input';
import { LogService } from '../log.service';
import { BaseCommand } from './base.command';
import { chunk } from 'lodash';

interface CommandOptions {
  source: StorageConfigInput;
  target: StorageConfigInput;
}

@Command({ name: 'backup', description: 'make backup from source to target' })
export class BackupCommand extends BaseCommand implements CommandRunner {
  constructor(
    protected readonly logService: LogService,
    protected readonly configService: ConfigService,
  ) {
    super(logService, configService);
  }

  async execute(passedParam: string[], options: CommandOptions): Promise<void> {
    const { source, target } = this.createSourceAndTargetStorage(
      options.source,
      options.target,
    );

    this.logService.debug('begin uploading files');
    const asyncIterable = source.filesIterable();
    let filesChunkIndex = 0;
    for await (const files of asyncIterable) {
      filesChunkIndex++;
      this.logService.log(
        `processing chunk ${filesChunkIndex} with ${files.length} items`,
      );

      const chunked = chunk(files, 10);
      for (let i = 0; i < chunked.length; i++) {
        this.logService.debug(`concurrent chunk ${i}`);
        await Promise.all(
          chunked[i].map(
            async (file) => await this.backupFile(file, target, options),
          ),
        );
      }
    }
  }

  private async backupFile(
    file: StorageFile,
    target: Storage,
    options: CommandOptions,
  ) {
    try {
      if (options.source.exclude) {
        const shouldPass = options.source.exclude.every(
          (ex) => file.path.indexOf(ex) === -1,
        );
        if (!shouldPass) {
          this.logService.debug(`${file.path} excluded`);
          return;
        }
      }

      this.logService.debug(`checking ${file.path} existence`);
      const hasFile = await target.hasFile(file);
      if (hasFile) {
        this.logService.debug(
          `file ${file.path} already exists. skip uploading`,
        );
        return;
      }
      this.logService.log(`uploading ${file.path}`);
      await target.putFile(file);
      this.logService.log(`${file.path} uploaded`);
    } catch (error) {
      this.logService.error(`error while uploading ${file.path}`);
      this.logService.error(error, error.stack);
    }
  }
}
