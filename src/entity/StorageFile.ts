import { Readable } from 'stream';

export abstract class StorageFile {
  constructor(readonly path: string) {}

  isEqual(other: StorageFile) {
    return this.path === other.path;
  }

  abstract size(): Promise<number>;

  abstract createReadableStream(chunkSize?: number): Promise<Readable>;
}
